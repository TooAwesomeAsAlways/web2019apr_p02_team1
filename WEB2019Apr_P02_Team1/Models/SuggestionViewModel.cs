﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2019Apr_P02_Team1.DAL;

namespace WEB2019Apr_P02_Team1.Models
{
    public class SuggestionViewModel
    {
        public int SuggestionID { get; set; }

        public int LecturerID { get; set; }

        public int StudentID { get; set; }

        [StringLength(3000)]
        public string Description { get; set; }

        // [Required]
        public char Status { get; set; }

        // [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }

        // Additional Attributes
        public string StudentName { get; set; }
    }
}
