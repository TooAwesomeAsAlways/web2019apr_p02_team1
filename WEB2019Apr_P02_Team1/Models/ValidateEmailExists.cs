﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2019Apr_P02_Team1.DAL;

namespace WEB2019Apr_P02_Team1.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private StudentDAL studentContext = new StudentDAL();
        private LecturerDAL lecturerContext = new LecturerDAL();
        private string _userType;

        public ValidateEmailExists(string userType)
        {
            _userType = userType;
        }

        public override bool IsValid(object value)
        {
            string emailAddr = Convert.ToString(value);
            switch (_userType)
            {
                case "Student":
                    return !studentContext.IsEmailExists(emailAddr);

                case "Lecturer":
                    return !lecturerContext.IsEmailExists(emailAddr);

                default:
                    throw new Exception("Invalid User Type");
            }
        }
    }
}
