﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2019Apr_P02_Team1.Models
{
    public class ProfileViewModel
    {
        public string Name { get; set; }

        [StringLength(50)]
        public string Course { get; set; }

        [StringLength(250)]
        public string Photo { get; set; }

        [StringLength(3000)]
        public string Description { get; set; }

        [StringLength(3000)]
        public string Achievement { get; set; }

        [Display(Name="Skill Sets")]
        public List<SkillSet> SkillSetList { get; set; }

        [StringLength(255)]
        public string ExternalLink { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [StringLength(50)]
        [EmailAddress]
        //[ValidateEmailExists(ErrorMessage = "Email address already exists!")]
        public string EmailAddr { get; set; }
    }
}
