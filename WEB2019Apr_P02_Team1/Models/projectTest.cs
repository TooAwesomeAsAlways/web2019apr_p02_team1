﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2019Apr_P02_Team1.Models
{
    public class ProjectTest
    {
        public int ProjectID { get; set; }

        public int StudentID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(maximumLength: 50)]
        //[ValidateMemberType(ErrorMessage = "Value must be either 'Member' or 'Leader' ")]
        public string Role { get; set; }

        public bool Check { get; set; }

        [Required]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your title to 255 characters or less")]
        public string Title { get; set; }

        [StringLength(maximumLength: 3000, ErrorMessage = "Please restrict your description to 3000 characters or less")]
        public string Description { get; set; }

        [Display(Name = "Project poster name")]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your project poster name to 255 characters or less")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Url")]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your url to 255 characters or less")]
        public string ProjectURL { get; set; }
    }
}
