﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2019Apr_P02_Team1.Models
{
    public class EditProjectViewModel
    {
        public Project Project { get; set; }
        public List<ProjectMember> Members { get; set; }
        public List<Student> MemberDetails { get; set; }
    }
}
