﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2019Apr_P02_Team1.DAL;

namespace WEB2019Apr_P02_Team1.Models
{
    public class ValidateMemberType : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            if (value.ToString().ToLower() != "member" || value.ToString().ToLower() != "leader")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
