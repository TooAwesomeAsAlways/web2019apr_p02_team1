﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2019Apr_P02_Team1.Models
{
    public class Password
    {
        [Display(Name = "Current Password")]
        [Required]
        [StringLength(255)]
        public string CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [Required]
        [StringLength(255)]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm Password")]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }
}
