﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.Models
{
    public class Lecturer
    {
        public int LecturerID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress]
        [StringLength(50)]
        public string EmailAddr { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }

        [StringLength(3000)]
        public string Description { get; set; }
    }
}
