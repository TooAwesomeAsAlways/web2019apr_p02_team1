﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2019Apr_P02_Team1.Models
{
    public class User
    {
        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress]
        [StringLength(50)]
        public string EmailAddr { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }
    }
}
