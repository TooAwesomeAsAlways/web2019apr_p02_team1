﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.Models
{
    public class SkillSet
    {
        public int SkillSetID { get; set; }

        public string SkillSetName { get; set; }

        public bool IsChecked { get; set; }
    }
}
