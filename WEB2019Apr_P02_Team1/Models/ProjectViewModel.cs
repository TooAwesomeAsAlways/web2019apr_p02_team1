﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WEB2019Apr_P02_Team1.Models
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }

        public IFormFile FileToUpload { get; set; }

        [Required]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your title to 255 characters or less")]
        public string Title { get; set; }

        [StringLength(maximumLength: 3000, ErrorMessage = "Please restrict your description to 3000 characters or less")]
        public string Description { get; set; }

        [Display(Name = "Project poster name")]
        //[RegularExpression("([^\\s]+(\\.(?i)(jpg||jpeg||jfif||pjpeg||pjp))$)")]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your project poster name to 255 characters or less")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Url")]
        [StringLength(maximumLength: 255, ErrorMessage = "Please restrict your url to 255 characters or less")]
        public string ProjectURL { get; set; }


    }
}
