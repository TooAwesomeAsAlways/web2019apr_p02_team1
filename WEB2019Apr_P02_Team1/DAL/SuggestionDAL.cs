﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.DAL
{
    public class SuggestionDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public SuggestionDAL()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                "EportfolioConnectionString");

            conn = new SqlConnection(strConn);
        }

        public int Create(Suggestion suggestion)
        {
            SqlCommand cmd = new SqlCommand(
                "INSERT INTO Suggestion(LecturerID, StudentID, Description)\n" +
                "VALUES (@lecturerID, @studentID, @description);", conn);

            cmd.Parameters.AddWithValue("@lecturerID", suggestion.LecturerID);
            cmd.Parameters.AddWithValue("@studentID", suggestion.StudentID);
            cmd.Parameters.AddWithValue("@description", suggestion.Description);

            conn.Open();
            int rowsAffected = (int)cmd.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;
        }

        public List<Suggestion> SearchByLecturer(Lecturer lecturer)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Suggestion\n" +
                "WHERE LecturerID = @lecturerID;", conn);

            cmd.Parameters.AddWithValue("@lecturerID", lecturer.LecturerID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Suggestions");
            conn.Close();

            List<Suggestion> suggestions = ConvertDataSet2Suggestions(result, "Suggestions");
            return suggestions;
        }

        public List<Suggestion> GetAll()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion;", conn);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Suggestions");
            conn.Close();

            List<Suggestion> suggestions = ConvertDataSet2Suggestions(result, "Suggestions");

            return suggestions;
        }

        public List<Suggestion> GetStudentSuggestions(int id)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion\n" +
                "WHERE StudentID = @studentId;", conn);
            cmd.Parameters.AddWithValue("@studentId", id);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result,"Suggestions");

            conn.Close();

            List<Suggestion> suggestions = ConvertDataSet2Suggestions(result, "Suggestions");

            return suggestions;
        }

        public int AcknowledgeSuggestion(int id)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Suggestion\n" +
                "SET Status = 'Y'\n" +
                "WHERE SuggestionID = @suggestionID", conn);

            cmd.Parameters.AddWithValue("@suggestionID", id);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        // Functions
        private List<Suggestion> ConvertDataSet2Suggestions(DataSet dataset, string tableName)
        {
            List<Suggestion> suggestions = new List<Suggestion>();
            foreach (DataRow row in dataset.Tables[tableName].Rows)
            {
                suggestions.Add(ConvertDataRow2Suggestion(row));
            }
            return suggestions;
        }

        private Suggestion ConvertDataRow2Suggestion(DataRow row)
        {
            Suggestion suggestion = new Suggestion
            {
                SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                LecturerID = Convert.ToInt32(row["LecturerID"]),
                StudentID = Convert.ToInt32(row["StudentID"]),
                Description = Convert.ToString(row["Description"]),
                Status = Convert.ToChar(row["Status"]),
                DateCreated = Convert.ToDateTime(row["DateCreated"])
            };
            return suggestion;
        }
    }
}
