﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.DAL
{
    public class SkillSetDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public SkillSetDAL()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortFolioConnectionString");
            conn = new SqlConnection(strConn);
        }

        public List<SkillSet> GetSkillSets(int studentID)
        {
            SqlCommand cmd = new SqlCommand("SELECT S.* FROM SkillSet S " +
                "INNER JOIN StudentSkillSet SS " +
                "ON S.SkillSetID = SS.SkillSetID " +
                "WHERE StudentID = @studentID " +
                "ORDER BY SkillSetID ASC", conn);

            cmd.Parameters.AddWithValue("@studentID", studentID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result, "SkillSets");
            conn.Close();

            List<SkillSet> skillSets = ConvertDataSet2SkillSets(result, "SkillSets");

            return skillSets;
        }

        public List<SkillSet> GetAll()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet", conn);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result, "AllSkillSets");

            conn.Close();

            List<SkillSet> AllSkillSets = ConvertDataSet2SkillSets(result, "AllSkillSets");

            return AllSkillSets;
        }

        //Not in use
        public int UpdateSkill(int SkillSetID, int StudentID)
        {
            SqlCommand cmd = new SqlCommand("UPDATE StudentSkillSet " +
                "SET SKillSetID = @skillSetId" +
                "WHERE StudentID = @studentId", conn);
            cmd.Parameters.AddWithValue("@skillSetId", SkillSetID);
            cmd.Parameters.AddWithValue("@studentId", StudentID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
            
        }

        public int InsertSkill(int SkillSetID, int StudentID)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentSkillSet " +
                "VALUES(@studentId, @skillSetId)", conn);

            cmd.Parameters.AddWithValue("@studentId", StudentID);
            cmd.Parameters.AddWithValue("@skillSetId", SkillSetID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        public int DeleteAllSkills(int StudentID)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM StudentSkillSet " +
                "WHERE StudentID = @studentId", conn);

            cmd.Parameters.AddWithValue("@studentId", StudentID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        private List<SkillSet> ConvertDataSet2SkillSets(DataSet result, string name)
        {
            List<SkillSet> skillSets = new List<SkillSet>();

            foreach(DataRow row in result.Tables[name].Rows)
            {
                skillSets.Add(ConvertDataRow2SkillSet(row));
            }

            return skillSets;
        }

        private SkillSet ConvertDataRow2SkillSet(DataRow row)
        {
            SkillSet skillSet = new SkillSet
            {
                SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                SkillSetName = row["SkillSetName"].ToString(),
            };

            return skillSet;
        }

    }
}
