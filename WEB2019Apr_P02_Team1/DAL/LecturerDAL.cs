﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.DAL
{
    public class LecturerDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public LecturerDAL()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                "EportfolioConnectionString");

            conn = new SqlConnection(strConn);
        }

        public int Create(Lecturer lecturer)
        {
            SqlCommand cmd = new SqlCommand(
                "INSERT INTO Lecturer (Name, EmailAddr, Description)\n" +
                "VALUES (@name, @emailAddr, @description);", conn);

            cmd.Parameters.AddWithValue("@name", lecturer.Name);
            cmd.Parameters.AddWithValue("@emailAddr", lecturer.EmailAddr);
            cmd.Parameters.AddWithValue("@description", lecturer.Description);

            conn.Open();
            int rowsAffected = (int)cmd.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;
        }

        public Lecturer SearchCredentials(string emailAddr, string password)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Lecturer\n" +
                "WHERE  EmailAddr   = @emailAddr\n" +
                "AND    Password    = @password COLLATE Latin1_General_CS_AS;", conn);

            cmd.Parameters.AddWithValue("@emailAddr", emailAddr);
            cmd.Parameters.AddWithValue("@password", password);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Lecturers");
            conn.Close();

            DataRowCollection rows = result.Tables["Lecturers"].Rows;
            if (rows.Count > 0) // If not empty
            {
                return ConvertDataRow2Lecturer(rows[0]);
            }
            else
            {
                return null;
            }
        }

        public Lecturer Get(int lecturerID)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Lecturer\n" +
                "WHERE LecturerID = @lecturerID;", conn);

            cmd.Parameters.AddWithValue("@lecturerID", lecturerID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Lecturers");
            conn.Close();


            DataRowCollection rows = result.Tables["Lecturers"].Rows;
            if (rows.Count > 0) // If not empty
            {
                return ConvertDataRow2Lecturer(rows[0]);
            }
            else
            {
                return null;
            }
        }

        public int Update(Lecturer lecturer)
        {
            SqlCommand cmd = new SqlCommand(
                "UPDATE Lecturer\n" +
                "SET Name        = @name, " +
                "    EmailAddr   = @emailAddr, " +
                "    Description = @description\n" +
                "WHERE LecturerID = @lecturerID;", conn);

            cmd.Parameters.AddWithValue("@name", lecturer.Name);
            cmd.Parameters.AddWithValue("@emailAddr", lecturer.EmailAddr);
            cmd.Parameters.AddWithValue("@description", lecturer.Description);
            cmd.Parameters.AddWithValue("@lecturerID", lecturer.LecturerID);

            conn.Open();
            int rowsAffected = (int)cmd.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;
        }

        public int ChangePassword(int lecturerID, string password)
        {
            SqlCommand cmd = new SqlCommand(
                "UPDATE Lecturer\n" +
                "SET    Password   = @password\n" +
                "WHERE  LecturerID = @lecturerID;", conn);

            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@lecturerID", lecturerID);

            conn.Open();
            int rowsAffected = (int)cmd.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;
        }

        public List<Lecturer> GetAll()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer", conn);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Lecturers");
            conn.Close();

            List<Lecturer> lecturers = ConvertDataSet2Lecturers(result, "Lecturers");
            return lecturers;
        }

        public bool IsEmailExists(string emailAddr)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Lecturer " +
                "WHERE EmailAddr = @emailAddr", conn);

            cmd.Parameters.AddWithValue("@emailAddr", emailAddr);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result, "IsEmailExist");

            conn.Close();

            if (result.Tables["IsEmailExist"].Rows.Count > 0)
                return true;
            else
                return false;
        }

        // Private Functions
        private List<Lecturer> ConvertDataSet2Lecturers(DataSet dataset, string tableName)
        {
            List<Lecturer> lecturers = new List<Lecturer>();
            foreach (DataRow row in dataset.Tables[tableName].Rows)
            {
                lecturers.Add(ConvertDataRow2Lecturer(row));
            }
            return lecturers;
        }

        private Lecturer ConvertDataRow2Lecturer(DataRow row)
        {
            Lecturer lecturer = new Lecturer
            {
                LecturerID = Convert.ToInt32(row["LecturerID"]),
                Name = Convert.ToString(row["Name"]),
                EmailAddr = Convert.ToString(row["EmailAddr"]),
                Password = Convert.ToString(row["Password"]),
                Description = Convert.ToString(row["Description"])
            };
            return lecturer;
        }
    }
}
