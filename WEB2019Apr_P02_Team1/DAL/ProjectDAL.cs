﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using Microsoft.AspNetCore.Http;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.DAL
{
    public class ProjectDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        private StudentDAL StudentContext = new StudentDAL();
        //contructor
        public ProjectDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            // Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortFolioConnectionString");

            //Instantiate a SqlConnection object with the 
            //Connection String method  * public SqlConnection(string connectionString); *

            //Connection used to access sql server database
            conn = new SqlConnection(strConn);
        }
        
        //Ok time to apply DRY as well
        //To create a Project
        public int Create(Project project)
        {   
            SqlCommand cmd1 = new SqlCommand("INSERT INTO Project (Title, Description, ProjectPoster, ProjectURL) " +
                "OUTPUT INSERTED.ProjectID " +
                "VALUES(@title, @description, @projectPoster, @projectURL)", conn);

            cmd1.Parameters.AddWithValue("@title",project.Title);

            if (project.Description == null)
            {
               cmd1.Parameters.AddWithValue("@description", DBNull.Value);
            }
            else
            {
                cmd1.Parameters.AddWithValue("@description", project.Description);
            }

            if (project.ProjectPoster == null)
            {
                cmd1.Parameters.AddWithValue("@projectPoster", DBNull.Value);
            }
            else
            {
                cmd1.Parameters.AddWithValue("@projectPoster", project.ProjectPoster);
            }

            if (project.ProjectURL == null)
            {
                cmd1.Parameters.AddWithValue("@projectURL", DBNull.Value);
            }
            else
            {
                cmd1.Parameters.AddWithValue("@projectURL", project.ProjectURL);
            }

            conn.Open();
            project.ProjectId = (int)cmd1.ExecuteScalar();
            conn.Close();

            return project.ProjectId;
        }


        //to get all of the projects
        public List<Project> GetAllProjectsHome()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM PROJECT", conn);

            SqlDataAdapter daProject = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "ProjectList");
            conn.Close();
            List<Project> ProjectList = ConvertProjects(result, "ProjectList");
            return ProjectList;
        }

        //to get all of the projects of the student
        public List<Project> GetAllProjects(int? id)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM PROJECT WHERE ProjectID in" +
                " (SELECT PROJECTID FROM PROJECTMEMBER WHERE STUDENTID = @memberID) ORDER BY PROJECTID ASC", conn);

            cmd.Parameters.AddWithValue("@memberID", id);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();
            conn.Open();

            daProject.Fill(result, "ProjectList");

            conn.Close();
            List<Project> ProjectList = ConvertProjects(result, "ProjectList");
            return ProjectList;
        }

        public Project GetProject(int? Projectid)
        {
            SqlCommand cmd = new SqlCommand("Select * FROM Project WHERE Project.ProjectID = @selectedProject", conn);
            cmd.Parameters.AddWithValue("@selectedProject", Projectid);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();
            daProject.Fill(result, "Project");
            conn.Close();

            if (result.Tables["Project"].Rows.Count > 0)
            {
                return ConvertProject(result.Tables["Project"].Rows[0]);
            }
            else
            {
                return null;
            }

        }

        public List<ProjectMember> GetMembers(int? id)
        {
            SqlCommand cmd = new SqlCommand("Select * FROM ProjectMEMBER WHERE ProjectMEMBER.ProjectID = @selectedProject", conn);
            cmd.Parameters.AddWithValue("@selectedProject", id);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "ProjectMembers");
            conn.Close();

            List<ProjectMember> members = ConvertMembers(result, "ProjectMembers");
            return members;
        }

        public int Update(Project project)
        {
            SqlCommand cmd = new SqlCommand("UPDATE PROJECT SET Description=@description, ProjectPoster=@poster, ProjectURL=@URL WHERE PROJECTID=@selectedProjID", conn);
            cmd.Parameters.AddWithValue("@selectedProjID", project.ProjectId);

            if (project.Description == null)
            {
                cmd.Parameters.AddWithValue("@description", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@description", project.Description);
            }
            
            if (project.ProjectURL == null)
            {
                cmd.Parameters.AddWithValue("@URL", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@URL", project.ProjectURL);
            }
            if (project.ProjectPoster == null)
            {
                cmd.Parameters.AddWithValue("@poster", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@poster", project.ProjectPoster);
            }

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        
        }

        //Delete A member from a Projectgroup
        public int Delete (int projectMemberID, int projectID)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM PROJECTMEMBER " +
            "WHERE StudentID = @selectMemberID AND ProjectID = @projID", conn);
            cmd.Parameters.AddWithValue("@selectMemberID", projectMemberID);
            cmd.Parameters.AddWithValue("@projID", projectID);
            //Open a database connection.
            conn.Open();
            int rowCount;
            rowCount = cmd.ExecuteNonQuery();
            //Close database connection.
            conn.Close();
            return rowCount;

        }

        public int AddMember (ProjectMember member)
        {
            SqlCommand cmd = new SqlCommand(
            "INSERT INTO ProjectMember(ProjectID, StudentID, Role) " +
            "OUTPUT INSERTED.ProjectID " +
            " VALUES(@projID, @stuID, @role)", conn);

            //define the parameters used in the sql statment, value for each parameter
            //is retrived from respective's class property
            cmd.Parameters.AddWithValue("@projID", member.ProjectID);
            cmd.Parameters.AddWithValue("@stuID", member.StudentID);
            cmd.Parameters.AddWithValue("@role", member.Role);
            

            //A connection to database must be opened before any operations made
            conn.Open();

            int id = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations
            conn.Close();

            //return id when no errors occurs
            return id;
        }


        public EditProjectViewModel GetMembersTest(int? id)
        {
            SqlCommand cmd = new SqlCommand("Select * FROM ProjectMEMBER p INNER JOIN Project pp on p.ProjectID = pp.ProjectID " +
                "INNER JOIN Student s on p.StudentID = s.StudentID " +
                "WHERE pp.ProjectID = @selectedProject", conn);
            cmd.Parameters.AddWithValue("@selectedProject", id);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "ProjectMembersTest");
            conn.Close();

            EditProjectViewModel members = ConvertMembersTest(result, "ProjectMembersTest");
            return members;
        }


        /// <summary>
        /// Functions used to simplify the process
        /// </summary>
        /// 

        //Projects
        private List<Project> ConvertProjects(DataSet set, string tableName)
        {
            List<Project> projects = new List<Project>();
            foreach (DataRow row in set.Tables[tableName].Rows)
            {
                Project project = ConvertProject(row);
                projects.Add(project);
            }
            return projects;
        }


        private Project ConvertProject(DataRow row)
        {
            Project project = new Project
            {
                ProjectId = Convert.ToInt32(row["ProjectId"]),
                Title = row["Title"].ToString(),
                Description = row["Description"].ToString(),
                ProjectPoster = row["ProjectPoster"].ToString(),
                ProjectURL = row["ProjectURL"].ToString(),
            };
            return project;
        }

        //Members
        private List<ProjectMember> ConvertMembers(DataSet set, string tableName)
        {
            List<ProjectMember> members = new List<ProjectMember>();
            foreach (DataRow row in set.Tables[tableName].Rows)
            {
                ProjectMember project = ConvertMember(row);
                members.Add(project);
            }
            return members;
        }

        private ProjectMember ConvertMember(DataRow row)
        {
            ProjectMember member = new ProjectMember
            {
                ProjectID = Convert.ToInt32(row["ProjectId"]),
                StudentID = Convert.ToInt32(row["StudentID"]),
                Role = row["Role"].ToString()
            };
            return member;
        }

        //Testing purposes
        private EditProjectViewModel ConvertMembersTest(DataSet set, string tableName)
        {
            EditProjectViewModel projectTests = new EditProjectViewModel();
            List<ProjectMember> members = new List<ProjectMember>();
            List<Student> students = new List<Student>();
            

            //This has all of the tables of students, projects and project members 
            //And converting them to their repective classes

            //Each row has the information of that one student
            foreach (DataRow row in set.Tables[tableName].Rows)
            {
                projectTests.Project = ConvertProject(row);
                ProjectMember member = ConvertMember(row);
                members.Add(member);
                Student student = ConvertDataRow2Student(row);
                students.Add(student);
            }

            projectTests.Members = members;
            projectTests.MemberDetails = students;
            return projectTests;
        }

        //not used
        /*
        private ProjectTest ConvertTest(DataRow row)
        {
            ProjectTest memberTest = new ProjectTest
            {
                ProjectID = Convert.ToInt32(row["ProjectId"]),
                StudentID = Convert.ToInt32(row["StudentID"]),
                Name = row["Name"].ToString(),
                Role = row["Role"].ToString(),
                Title = row["Title"].ToString(),
                Description = row["Description"].ToString(),
                ProjectPoster = row["ProjectPoster"].ToString(),
                ProjectURL = row["ProjectURL"].ToString(),
            };
            return memberTest;
        }
         */


        private Student ConvertDataRow2Student(DataRow row)
        {
            Student student = new Student
            {
                StudentID = Convert.ToInt32(row["StudentID"]),
                Name = row["Name"].ToString(),
                Course = row["Course"].ToString(),
                Photo = row["Photo"].ToString(),
                Description = row["Description"].ToString(),
                Achievement = row["Achievement"].ToString(),
                ExternalLink = row["ExternalLink"].ToString(),
                EmailAddr = row["EmailAddr"].ToString(),
                Password = row["Password"].ToString(),
                MentorID = Convert.ToInt32(row["MentorID"])
            };
            return student;
        }
    }


}
