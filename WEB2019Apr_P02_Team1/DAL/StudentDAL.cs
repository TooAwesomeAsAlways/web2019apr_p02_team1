﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WEB2019Apr_P02_Team1.Models;


namespace WEB2019Apr_P02_Team1.DAL
{
    public class StudentDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //contructor
        public StudentDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            // Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortFolioConnectionString");

            //Instantiate a SqlConnection object with the 
            //Connection String method  * public SqlConnection(string connectionString); *

            //Connection used to access sql server database
            conn = new SqlConnection(strConn);
        }

        public int Create(Student student)
        {
            SqlCommand cmd = new SqlCommand(
                "INSERT INTO Student (Name, Course, EmailAddr, MentorID)\n" +
                "OUTPUT INSERTED.StudentID\n" +
                "VALUES(@name, @course, @email, @mentorId)", conn);

            cmd.Parameters.AddWithValue("@name", student.Name);
            cmd.Parameters.AddWithValue("@course", student.Course);
            cmd.Parameters.AddWithValue("@email", student.EmailAddr);
            cmd.Parameters.AddWithValue("@mentorId", student.MentorID);

            conn.Open();

            student.StudentID = (int)cmd.ExecuteScalar();

            conn.Close();

            return student.StudentID;
        
        }

        //Just added this to test if the databse works
        //feel free to use this func
        public List<Student> GetAllStudents()
        {
            SqlCommand cmd = new SqlCommand("Select * FROM Student ORDER BY StudentID", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "Students");

            conn.Close();

            //May need to add some validation idk
            List<Student> StudentList = ConvertDataSet2Students(result, "Students");
            return StudentList;
        }

        // For Lecturers
        public List<Student> SearchByLecturer(Lecturer lecturer)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Student\n" +
                "WHERE MentorID = @mentorID", conn);

            cmd.Parameters.AddWithValue("@mentorID", lecturer.LecturerID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Students");
            conn.Close();

            List<Student> students = ConvertDataSet2Students(result, "Students");

            return students;
        }

        public Student Get(int studentID)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Student\n" +
                "WHERE StudentID = @studentID;", conn);

            cmd.Parameters.AddWithValue("@studentID", studentID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(result, "Students");
            conn.Close();

            DataRowCollection rows = result.Tables["Students"].Rows;
            if (rows.Count > 0)
            {
                return ConvertDataRow2Student(rows[0]);
            }
            else
            {
                return null;
            }
        }

        public int Update(ProfileViewModel profile, int id)
        {
            SqlCommand cmd = new SqlCommand(
                "UPDATE Student SET Description = @description, " +
                "Achievement = @achievement, ExternalLink = @externalLink, EmailAddr = @emailAddr\n" +
                "WHERE StudentID = @studentId", conn);

            if (profile.Description != null)
                cmd.Parameters.AddWithValue("@description", profile.Description);
            else
                cmd.Parameters.AddWithValue("@description", DBNull.Value);
            if (profile.Achievement != null)
                cmd.Parameters.AddWithValue("@achievement", profile.Achievement);
            else
                cmd.Parameters.AddWithValue("@achievement", DBNull.Value);
            if (profile.ExternalLink != null)
                cmd.Parameters.AddWithValue("@externalLink", profile.ExternalLink);
            else
                cmd.Parameters.AddWithValue("@externalLink", DBNull.Value);
            cmd.Parameters.AddWithValue("@emailAddr", profile.EmailAddr);
            cmd.Parameters.AddWithValue("@studentId", id);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        public int UpdatePicture(Student student)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Photo = @photo\n" +
                "WHERE StudentID = @studentId", conn);

            cmd.Parameters.AddWithValue("@photo", student.Photo);
            cmd.Parameters.AddWithValue("@studentId", student.StudentID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        public Student SearchCredentials(string emailAddr, string passID)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM STUDENT\n" +
                "WHERE  EmailAddr   = @emailAddr\n" +
                "AND    Password    = @password COLLATE Latin1_General_CS_AS;", conn);

            cmd.Parameters.AddWithValue("@emailAddr", emailAddr);
            cmd.Parameters.AddWithValue("@password", passID);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result, "StudentCredentials");
            conn.Close();

            DataRowCollection rows = result.Tables["StudentCredentials"].Rows;
            if (rows.Count > 0)
            {
                return ConvertDataRow2Student(rows[0]);
            }
            else
            {
                return null;
            }
        }

        public bool IsEmailExists(string emailAddr)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Student " +
                "WHERE EmailAddr = @emailAddr", conn);

            cmd.Parameters.AddWithValue("@emailAddr", emailAddr);

            DataSet result = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();

            da.Fill(result, "IsEmailExist");

            conn.Close();

            if (result.Tables["IsEmailExist"].Rows.Count > 0)
                return true;
            else
                return false;
        }

        // Functions
        private List<Student> ConvertDataSet2Students(DataSet dataset, string tableName)
        {
            List<Student> students = new List<Student>();
            foreach (DataRow row in dataset.Tables[tableName].Rows)
            {
                students.Add(ConvertDataRow2Student(row));
            }
            return students;
        }

        private Student ConvertDataRow2Student(DataRow row)
        {
            Student student = new Student
            {
                StudentID = Convert.ToInt32(row["StudentID"]),
                Name = row["Name"].ToString(),
                Course = row["Course"].ToString(),
                Photo = row["Photo"].ToString(),
                Description = row["Description"].ToString(),
                Achievement = row["Achievement"].ToString(),
                ExternalLink = row["ExternalLink"].ToString(),
                EmailAddr = row["EmailAddr"].ToString(),
                Password = row["Password"].ToString(),
                MentorID = Convert.ToInt32(row["MentorID"])
            };
            return student;
        }
    }
}
