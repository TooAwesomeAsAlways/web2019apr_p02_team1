﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEB2019Apr_P02_Team1.Models;
using WEB2019Apr_P02_Team1.DAL;

namespace WEB2019Apr_P02_Team1.Controllers
{
    public class DebugController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();
        private StudentDAL studentContext = new StudentDAL();
        private SuggestionDAL suggestionContext = new SuggestionDAL();

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult DebugViewLecturers()
        {
            List<Lecturer> lecturers = lecturerContext.GetAll();
            return View(lecturers);
        }

        public ActionResult DebugViewStudents()
        {
            List<Student> students = studentContext.GetAllStudents();
            return View(students);
        }

        public ActionResult DebugViewSuggestions()
        {
            List<Suggestion> suggestions = suggestionContext.GetAll();
            return View(suggestions);
        }
    }
}