﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using WEB2019Apr_P02_Team1.DAL;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.Controllers
{
    public class StudentController : Controller
    {
        private StudentDAL studentContext = new StudentDAL();
        private LecturerDAL lecturerContext = new LecturerDAL();
        private SkillSetDAL skillSetContext = new SkillSetDAL();
        private SuggestionDAL suggestionContext = new SuggestionDAL();

        //Display student profile
        public ActionResult ViewProfile(int? id)
        {
            if (id == null)
            {
                id = HttpContext.Session.GetInt32("ID"); //Check if it is a student
                if (id == null) //If there is no id in session (anonymous)
                {
                    return RedirectToAction("Students", "Home");
                }
            }
            Student student = studentContext.Get(id.Value);

            if(student != null)
            {
                List<SkillSet> skillSets = skillSetContext.GetSkillSets(id.Value);
                ProfileViewModel profile = MapToProfileVM(student, skillSets);
                return View(profile);
            }
            else
            {
                return RedirectToAction("Students", "Home");
            }
        }

        public IActionResult Index()
        {
            if((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)HttpContext.Session.GetInt32("ID");
            Student student = studentContext.Get(id);
            ViewData["StudentName"] = student.Name;

            return View();
        }

        //Create student profile
        public ActionResult Register()
        {
            Student student = new Student();
            List<Lecturer> lecturers = lecturerContext.GetAll();
            ViewData["CourseList"] = GetCourse();
            ViewData["LecturerNames"] = GetLecturerNames(lecturers);
            return View(student);
        }

        private List<SelectListItem> GetCourse()
        {
            List<SelectListItem> courseList = new List<SelectListItem>();
            courseList.Add(new SelectListItem
            {
                Value = "IT",
                Text = "IT"
            });
            courseList.Add(new SelectListItem
            {
                Value = "FI",
                Text = "FI"
            });

            return courseList;
        }

        private List<SelectListItem> GetLecturerNames(List<Lecturer> lecturers)
        {
            List<SelectListItem> lecturerNames = new List<SelectListItem>();
            foreach(Lecturer lecturer in lecturers)
            {
                lecturerNames.Add(new SelectListItem
                {
                    Value = lecturer.LecturerID.ToString(),
                    Text = lecturer.Name
                });
            }

            return lecturerNames;
        }

        [HttpPost]
        public ActionResult Register(Student student)
        {
            if (ModelState.IsValid)
            {
                if (studentContext.IsEmailExists(student.EmailAddr))
                {
                    List<Lecturer> lecturers = lecturerContext.GetAll();
                    ViewData["CourseList"] = GetCourse();
                    ViewData["LecturerNames"] = GetLecturerNames(lecturers);
                    ViewData["Message"] = "Email Already Exists"; //More eleagant pls
                    return View(student);
                }
                student.MentorID = Convert.ToInt32(student.MentorID);
                student.StudentID = studentContext.Create(student);
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Lecturer> lecturers = lecturerContext.GetAll();
                ViewData["CourseList"] = GetCourse();
                ViewData["LecturerNames"] = GetLecturerNames(lecturers);
                return View(student);
            }
        }

        private ProfileViewModel MapToProfileVM(Student student, List<SkillSet> skillSets)
        {
            ProfileViewModel profile = new ProfileViewModel
            {
                Name = student.Name,
                Course = student.Course,
                Photo = student.Photo,
                Description = student.Description,
                Achievement = student.Achievement,
                SkillSetList = skillSets,
                ExternalLink = student.ExternalLink,
                EmailAddr = student.EmailAddr

            };

            return profile;
        }

        public ActionResult Update()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)HttpContext.Session.GetInt32("ID");
            Student student = studentContext.Get(id);
            List<SkillSet> oldSkillSets = skillSetContext.GetSkillSets(id);
            List<SkillSet> skillSets = skillSetContext.GetAll();

            if(oldSkillSets.Count > 0) //If there is no existing skillsets
            {
                int count = 0;
                foreach (SkillSet s in skillSets)
                {
                    if (oldSkillSets[count].SkillSetID == s.SkillSetID)
                    {
                        s.IsChecked = true;
                        count++;
                    }
                    if (count == oldSkillSets.Count) //Break when finishes
                        break;
                }
            }

            ProfileViewModel profile = MapToProfileVM(student, skillSets); //Take note
            return View(profile);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ProfileViewModel profile)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                HttpContext.Session.GetString("Role") != "Student")
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)HttpContext.Session.GetInt32("ID");
            Student student = studentContext.Get(id);

            if (!ModelState.IsValid)
            {
                ViewData["Message"] = "Update Fail. Pls try agian";
                return View(profile);
            }
            if (student.EmailAddr != profile.EmailAddr) //Email Change
            {
                if (studentContext.IsEmailExists(profile.EmailAddr)) //Email exists
                {
                    ViewData["Message"] = "Email Address has already existed!";
                    return View(profile);
                }
            }
            studentContext.Update(profile, id);
            skillSetContext.DeleteAllSkills(id);

            foreach (SkillSet s in profile.SkillSetList)
            {
                if (s.IsChecked)
                {
                    skillSetContext.InsertSkill(s.SkillSetID, id);
                }
            }

            ViewData["Success"] = "Update Successful";
            return View(profile);

        }

        public ActionResult UploadPhoto()
        {
            if((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)HttpContext.Session.GetInt32("ID");
            Student student = studentContext.Get(id);
            return View(student);
        }

        [HttpPost]
        public async Task<IActionResult> UploadPhoto(Student student)
        {
            if((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            if(student.FileToUpload != null && student.FileToUpload.Length > 0)
            {
                List<string> extenstionTypes = new List<string> { ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp" };

                string fileExt = Path.GetExtension(student.FileToUpload.FileName);

                if(!extenstionTypes.Contains(fileExt))
                {
                    ViewData["Message"] = "Invalid file type. Please use one of the following. (.jpg, .jpeg, .jfif, .pjpeg, .pjp)";
                    return View(student);
                }

                try
                {
                    string uploadedFile = student.StudentID + fileExt;

                    string savePath = Path.Combine(Directory.GetCurrentDirectory(),
                        "wwwroot\\images", uploadedFile);

                    using (var fileStream = new FileStream(
                        savePath, FileMode.Create))
                    {
                        await student.FileToUpload.CopyToAsync(fileStream);
                    }

                    student.Photo = uploadedFile;
                    studentContext.UpdatePicture(student);
                    ViewData["Success"] = "File upload successfully";
                }
                catch(IOException)
                {
                    ViewData["Message"] = "File uploading fail!";
                }
                catch(Exception ex)
                {
                    ViewData["Message"] = ex.Message;
                }
            }

            return View(student);
        }

        public ActionResult ViewSuggestions()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)HttpContext.Session.GetInt32("ID");
            List<Suggestion> suggestions = suggestionContext.GetStudentSuggestions(id);

            return View(suggestions);
        }

        public ActionResult Acknowledge(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            if(id == null)
            {
                TempData["Message"] = "Acknowledge unsuccessful. Please try again."; 
                return RedirectToAction("ViewSuggestions");
            }

            int count = suggestionContext.AcknowledgeSuggestion(id.Value);
            TempData["Success"] = "Acknowledge successful.";
            return RedirectToAction("ViewSuggestions");
        }

    }
}