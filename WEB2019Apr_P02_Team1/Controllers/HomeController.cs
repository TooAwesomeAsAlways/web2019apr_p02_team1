﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WEB2019Apr_P02_Team1.DAL;
using WEB2019Apr_P02_Team1.Models;

namespace WEB2019Apr_P02_Team1.Controllers
{
    public class HomeController : Controller
    {
        private StudentDAL studentContext = new StudentDAL();
        private ProjectDAL ProjectContext = new ProjectDAL();
        private LecturerDAL LecturerContext = new LecturerDAL();

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Students()
        {
            List<Student> studentList = studentContext.GetAllStudents();
            return View(studentList);
        }

        public ActionResult Projects()
        {
            List<Project> projectList = ProjectContext.GetAllProjectsHome();
            return View(projectList);
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            Student student = studentContext.SearchCredentials(user.EmailAddr, user.Password);
            if (student != null)
            {
                HttpContext.Session.SetString("Role", "Student");
                HttpContext.Session.SetInt32("ID", student.StudentID);
                return RedirectToAction("Index", "Student");
            }

            Lecturer lecturer = LecturerContext.SearchCredentials(user.EmailAddr, user.Password);
            if (lecturer != null)
            {
                HttpContext.Session.SetString("Role", "Lecturer");
                HttpContext.Session.SetInt32("LecturerID", lecturer.LecturerID);
                return RedirectToAction("Index", "Lecturer");
            }

            ViewData["Message"] = "Invalid login Credentials";
            return View();
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Index");
        }

    }
}