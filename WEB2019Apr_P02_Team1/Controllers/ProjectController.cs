﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using WEB2019Apr_P02_Team1.DAL;
using WEB2019Apr_P02_Team1.Models;
using System.IO;

namespace WEB2019Apr_P02_Team1.Controllers
{
    public class ProjectController : Controller
    {
        public IActionResult Index()
        {
            int? id = HttpContext.Session.GetInt32("ID");
            string session = HttpContext.Session.GetString("Role");
            if (id == null || session != "Student")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();

        }

        private ProjectDAL ProjectContext = new ProjectDAL();
        private StudentDAL StudentContext = new StudentDAL();


        public ActionResult Create()
        {
            int? id = HttpContext.Session.GetInt32("ID");
            string session = HttpContext.Session.GetString("Role");
            if (id == null || session != "Student")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        //Creating the portfolio
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project)
        {
            if (ModelState.IsValid)
            {
                int? id = HttpContext.Session.GetInt32("ID");

                int projectId = ProjectContext.Create(project);
                Student student = StudentContext.Get(Convert.ToInt32(id));
                ProjectMember member = new ProjectMember
                {
                    ProjectID = projectId,
                    StudentID = student.StudentID,
                    Role = "Leader"
                };
                ProjectContext.AddMember(member);
                return RedirectToAction("ViewProject");
            }

            return View(project);
            
        }

        public ActionResult ViewProject()
        {
            int? id = HttpContext.Session.GetInt32("ID");
            string session = HttpContext.Session.GetString("Role");
            if (id == null || session != "Student")
            {
                return RedirectToAction("Index", "Home");
            }
            //Getting all of the projects of that student
            List<Project> ProjectList = ProjectContext.GetAllProjects(id);
            if (ProjectList.Count == 0 || ProjectList == null)
            {
                TempData["error"] = "No projects found, create one by clicking on the create project button";
                return View(ProjectList);
            }
            return View(ProjectList);
        }


        //Getting the details of the project
        public ActionResult DetailProject(int? id)
        {
            string session = HttpContext.Session.GetString("Role");
            if (id == null || ProjectContext.GetProject(id.Value) == null)
            {
                return RedirectToAction("Index", "Home");
            }
            EditProjectViewModel view = ProjectContext.GetMembersTest(id.Value);
            ViewData["role"] = session;
            return View(view);
        }


        public ActionResult UpdateNew(int? id)
        {
            int? idview = HttpContext.Session.GetInt32("ID");
            string session = HttpContext.Session.GetString("Role");

            if (idview == null || session != "Student" || id == null)
            {   
                return RedirectToAction("Index", "Home");
            }

            //var (studentList, memberDetail, project, RemainingStudents) = GetDetailsMember(id.Value);
            EditProjectViewModel view = ProjectContext.GetMembersTest(id.Value);
            //not letting other from viewing the content
            //make this into a function 
            List<int> ids = new List<int>();
            foreach (var item in view.MemberDetails)
            {
                ids.Add(item.StudentID);
            }
            if (!ids.Contains(Convert.ToInt32(idview)))
            {
              return RedirectToAction("Index", "Home");
            }

            List<Student> students = StudentContext.GetAllStudents();
            List<Student> nonMembersDetails = GetNonMembers(students, view.MemberDetails);

            //converts all non-members to members
            List<ProjectMember> convertedAllMembers = ConvertAllToProjectMember(nonMembersDetails, view.Members, view.Project);
            // however only people selected will be added/removed in the end
            EditProjectViewModel edit = new EditProjectViewModel
            {
                Project = view.Project,
                Members = convertedAllMembers,
                //Getting students who are not in the group to be added as options
                MemberDetails = StudentContext.GetAllStudents()
               
            };
            return View(edit);
        }

        [HttpPost]
        public ActionResult UpdateNew(EditProjectViewModel edit)
        {

            if (ModelState.IsValid)
            {
                List<ProjectMember> members = ProjectContext.GetMembers(edit.Project.ProjectId);
                ProjectContext.Update(edit.Project);
                foreach (var item in edit.Members)
                {
                    int count = 0;
                    if (item.Check)
                    {
                        foreach (var mem in members)
                        {
                            if (mem.StudentID == item.StudentID)
                            {
                                count += 1;
                                //remove all of the spaces so that we can check if the item is a member
                                item.Role = item.Role.Replace(" ", string.Empty);
                                if (item.Role == "Member")
                                {
                                    ProjectContext.Delete(item.StudentID, edit.Project.ProjectId);
                                }
                            }
                        }

                        if (count == 0)
                        {
                            item.Role = "Member";
                            ProjectContext.AddMember(item);
                        }
                    }
                }
                return RedirectToAction("ViewProject");
            }
            else
            {

                EditProjectViewModel view = ProjectContext.GetMembersTest(edit.Project.ProjectId);
                List<Student> students = StudentContext.GetAllStudents();
                List<Student> nonMembersDetails = GetNonMembers(students, view.MemberDetails);

                //converts all non-members to members
                List<ProjectMember> convertedAllMembers = ConvertAllToProjectMember(nonMembersDetails, view.Members, view.Project);

                // however only people selected will be added/removed in the end
                EditProjectViewModel edit1 = new EditProjectViewModel
                {
                    Project = view.Project,
                    Members = convertedAllMembers,
                    //Getting students who are not in the group to be added as options
                    MemberDetails = StudentContext.GetAllStudents()

                };

                TempData["Error"] = "Invalid input";
                //edit1 was here
                return View(edit1);
            }
        }

        public ActionResult Upload(int? id)
        {   
            //make this into a function too
            int? idview = HttpContext.Session.GetInt32("ID");
            string session = HttpContext.Session.GetString("Role");

            if (idview == null || session != "Student" || id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            EditProjectViewModel view = ProjectContext.GetMembersTest(id.Value);

            //not letting other from viewing the content
            List<int> ids = new List<int>();
            foreach (var item in view.MemberDetails)
            {
                ids.Add(item.StudentID);
            }
            if (!ids.Contains(Convert.ToInt32(idview)))
            {
                return RedirectToAction("Index", "Home");
            }

            ProjectViewModel convertedProject = ConvertProject(view.Project);
            return View(convertedProject);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(ProjectViewModel convertedProject)
        {
            //If the user does not have any upload and clicks on upload 
            //validate
            if (convertedProject.FileToUpload != null
               && convertedProject.FileToUpload.Length > 0) 
            {
                if (ModelState.IsValid)
                {
                    string fileExtension =
                            Path.GetExtension(convertedProject.FileToUpload.FileName);


                    if (!CheckExtension(fileExtension))
                    {
                        ViewData["Message"] = "Wrong file type, please use one of the following (.jpg, .jpeg, .jfif, .pjpeg, .pjp)";
                        Project project = ProjectContext.GetProject(convertedProject.ProjectId);
                        convertedProject.ProjectPoster = project.ProjectPoster;
                        return View(convertedProject);
                    }


                    try
                    {
                        //rename the file
                        string upload =
                            "Project_" + convertedProject.ProjectId + "_Poster" + fileExtension;

                        //get the absolute file path to the images folder in the server
                        string saveFile =
                                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", upload);


                        //upload the given file to the server
                        using (var fileStream = new FileStream(saveFile, FileMode.Create))
                        {
                            await convertedProject.FileToUpload.CopyToAsync(fileStream);
                        }

                        convertedProject.ProjectPoster = upload;
                        //we will need to convert the converted back to a project

                        Project updated = new Project
                        {
                            ProjectId = convertedProject.ProjectId,
                            Title = convertedProject.Title,
                            Description = convertedProject.Description,
                            ProjectPoster = convertedProject.ProjectPoster,
                            ProjectURL = convertedProject.ProjectURL,
                        };

                        ProjectContext.Update(updated);
                        ViewData["Success"] = "File uploaded";

                    }
                    catch (IOException)
                    {
                        ViewData["Message"] = "File upload fail";
                    }
                    catch (Exception ex)
                    {
                        ViewData["Message"] = ex.Message;
                    }
                }
            }
            else
            {
                Project project1 = ProjectContext.GetProject(convertedProject.ProjectId);
                convertedProject.ProjectPoster = project1.ProjectPoster;
                ViewData["Message"] = "Please select an image";
            }

            return View(convertedProject);
        }

        //converting the model
        private ProjectViewModel ConvertProject(Project project)
        {
            ProjectViewModel projectViewModel = new ProjectViewModel
            {
                ProjectId = project.ProjectId,
                Title = project.Title,
                Description = project.Description,
                ProjectPoster = project.ProjectPoster,
                ProjectURL = project.ProjectURL,
             };
            return projectViewModel;
        }

        //get all of the non members of a project so that we can list them in the table below the member table
        private List<Student> GetNonMembers(List<Student> allstudents , List<Student> members)
        {
            for (int i = 0; i < members.Count(); i++)
            {
                for (int j = 0; j < allstudents.Count(); j++)
                {
                    if (members[i].StudentID == allstudents[j].StudentID)
                    {
                        allstudents.Remove(allstudents[j]);
                    }
                }
            }
            List<Student> nonMembers = allstudents;
            return nonMembers;
        }
        
        //converting all non-members to members
        private List<ProjectMember> ConvertAllToProjectMember(List<Student> NonMembers, List<ProjectMember> current, Project project)
        {
            foreach (var item in NonMembers)
            {
                //Student details of students who are not in the group 
                //are added to the project group
                current.Add(
                    new ProjectMember
                    {
                        ProjectID = project.ProjectId,
                        StudentID = item.StudentID,
                        Role = ""
                    }
                 );
            }

            return current;
        }

        //checks file extensions (Public for student controller to check file type)
        public bool CheckExtension(string ext)
        {
            List<string> extenstionTypes = new List<string> { ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp" };
            if (!extenstionTypes.Contains(ext.ToLower()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
    }
}