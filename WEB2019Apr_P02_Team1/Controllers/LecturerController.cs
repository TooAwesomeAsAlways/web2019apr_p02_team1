﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WEB2019Apr_P02_Team1.Models;
using WEB2019Apr_P02_Team1.DAL;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace WEB2019Apr_P02_Team1.Controllers
{
    public class LecturerController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();
        private StudentDAL studentContext = new StudentDAL();
        private SuggestionDAL suggestionContext = new SuggestionDAL();

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);

            ViewData["LecturerName"] = lecturer.Name;

            return View();
        }

        public ActionResult Register()
        {
            Lecturer lecturer = new Lecturer
            {
                Password = "p@55Lecturer"
            };
            return View(lecturer);
        }

        [HttpPost]
        public ActionResult Register(Lecturer lecturer)
        {
            if (!ModelState.IsValid)
            {
                return View(lecturer);
            }

            if (lecturerContext.IsEmailExists(lecturer.EmailAddr))
            {
                ViewData["Message"] = "Email Already Exists";
                return View(lecturer);
            }

            if (lecturer.Description == null)
                lecturer.Description = "";

            lecturerContext.Create(lecturer);
            return RedirectToAction("Login", "Home");
        }

        //public ActionResult Login()
        //{
        //    Lecturer lecturer = new Lecturer
        //    {
        //        Name = "Required"
        //    };
        //    return View(lecturer);
        //}

        //[HttpPost]
        //public ActionResult Login(Lecturer lecturer)
        //{
        //    if (!ModelState.IsValid)
        //    {                
        //        return View(lecturer);
        //    }

        //    lecturer = lecturerContext.SearchCredentials(lecturer.EmailAddr, lecturer.Password);

        //    if (lecturer != null) // Checks if List Contains Elements
        //    {
        //        HttpContext.Session.SetString("Role", "Lecturer");
        //        HttpContext.Session.SetInt32("LecturerID", lecturer.LecturerID);
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        ViewData["Message"] = "Invalid Login Credentials";
        //        return View(lecturer);
        //    }
        //}

        public ActionResult PostSuggestion(int? defaultStudentID)
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);

            List<Student> students = studentContext.SearchByLecturer(lecturer);

            Student defaultStudent = null;
            foreach (Student student in students)
            {
                if (student.StudentID == defaultStudentID)
                {
                    defaultStudent = student;
                    break;
                }
            }

            List<SelectListItem> studentListItems;
            if (defaultStudent == null) studentListItems = ConvertStudents2ListItems(students);
            else                        studentListItems = new List<SelectListItem> { ConvertStudent2ListItem(defaultStudent) };

            ViewData["StudentList"] = studentListItems;

            Suggestion suggestion = new Suggestion();
            return View(suggestion);
        }

        [HttpPost]
        public ActionResult PostSuggestion(Suggestion suggestion)
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);

            List<Student> students = studentContext.SearchByLecturer(lecturer);
            List<SelectListItem> studentListItems = ConvertStudents2ListItems(students);
            ViewData["StudentList"] = studentListItems;

            if (!ModelState.IsValid)
            {
                return View(suggestion);
            }

            suggestion.LecturerID = lecturerID;
            suggestionContext.Create(suggestion);

            ViewData["Success"] = "Suggestion Posted Successfully.";

            suggestion = new Suggestion();
            return View();
        }

        public ActionResult ViewSuggestions()
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);
            List<Suggestion> suggestions = suggestionContext.SearchByLecturer(lecturer);
            List<SuggestionViewModel> suggestionsVM = new List<SuggestionViewModel>();
            foreach (Suggestion suggestion in suggestions)
                suggestionsVM.Add(MapToStaffVM(suggestion));

            return View(suggestionsVM);
        }

        public ActionResult ViewMentees()
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);

            List<Student> students = studentContext.SearchByLecturer(lecturer);

            return View(students);
        }

        public ActionResult UpdateProfile()
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);
            lecturer.Password = "Required";

            return View(lecturer);
        }

        [HttpPost]
        public ActionResult UpdateProfile(Lecturer newLecturer)
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            if (!ModelState.IsValid)
            {
                return View();
            }

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer oldLecturer = lecturerContext.Get(lecturerID);

            if (oldLecturer.EmailAddr != newLecturer.EmailAddr &&
                lecturerContext.IsEmailExists(newLecturer.EmailAddr))
            {
                ViewData["Message"] = "Email Already Exists";
                return View(newLecturer);
            }
            else
            {
                if (newLecturer.Description == null)
                    newLecturer.Description = "";

                newLecturer.LecturerID = lecturerID;

                lecturerContext.Update(newLecturer);

                ViewData["Success"] = "Profile Updated Successfully.";
                return View();
            }
        }

        public ActionResult ChangePassword()
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(Password password)
        {
            if (HttpContext.Session.GetString("Role") != "Lecturer")
                return RedirectToAction("Login", "Home");

            if (!ModelState.IsValid)
            {
                return View();
            }

            Debug.Assert(HttpContext.Session.GetInt32("LecturerID") != null);
            int lecturerID = (int)HttpContext.Session.GetInt32("LecturerID");

            Lecturer lecturer = lecturerContext.Get(lecturerID);

            if (lecturer.Password != password.CurrentPassword)
            {
                ViewData["Message"] = "Invalid Password";
                return View();
            }

            if (password.NewPassword.Length < 8 || !password.NewPassword.Any(char.IsDigit))
            {
                ViewData["Message"] = "New Password must contain at least 8 characters & a digit.";
                return View();
            }

            lecturerContext.ChangePassword(lecturerID, password.NewPassword);
            ViewData["Success"] = "Password Updated Successfully.";

            return View();
        }

        // Functions
        private SelectListItem ConvertStudent2ListItem(Student student)
        {
            SelectListItem studentListItem = new SelectListItem
            {
                Text = student.Name,
                Value = student.StudentID.ToString()
            };
            return studentListItem;
        }

        private List<SelectListItem> ConvertStudents2ListItems(List<Student> students)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (Student student in students)
            {
                listItems.Add(ConvertStudent2ListItem(student));
            }
            return listItems;
        }

        private SuggestionViewModel MapToStaffVM(Suggestion suggestion)
        {
            SuggestionViewModel suggestionVM = new SuggestionViewModel
            {
                SuggestionID = suggestion.SuggestionID,
                LecturerID = suggestion.LecturerID,
                StudentID = suggestion.StudentID,
                Description = suggestion.Description,
                Status = suggestion.Status,
                DateCreated = suggestion.DateCreated,
                StudentName = studentContext.Get(suggestion.StudentID).Name
            };
            return suggestionVM;
        }
    }
}